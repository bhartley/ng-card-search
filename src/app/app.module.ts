import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { CardDetailComponent } from './card-detail/card-detail.component';
import { CardTextSearchComponent } from './card-search/card-text-search.component';
import { CardSearchComponent } from './card-search/card-search.component';
import { WarningMessageComponent } from './warning-message/warning-message.component';

import { CardSearchService } from './card-search/card-search.service';
import { NewCardSearchService } from './card-search/new-card-search.service';
import { CardTextSearchService } from './card-search/card-text-search.service';
import { TabTypesComponent } from './card-detail/tab-types/tab-types.component';
import { TabTextComponent } from './card-detail/tab-text/tab-text.component';
import { TabManaColorsComponent } from './card-detail/tab-mana-colors/tab-mana-colors.component';

import { routes } from './routes';

@NgModule({
  declarations: [
    AppComponent,
    CardDetailComponent,
    CardSearchComponent,
    CardTextSearchComponent,
    WarningMessageComponent,
    TabTypesComponent,
    TabTextComponent,
    TabManaColorsComponent
  ],
  imports: [
    NgxPaginationModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    CardTextSearchService,
    CardSearchService,
    NewCardSearchService
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
