export const url = {
  card: 'https://node-card-server.herokuapp.com/card/',
  search: 'https://node-card-server.herokuapp.com/search/',
  searchText: 'https://node-card-server.herokuapp.com/searchText/'
};

export const devUrl = {
  card: 'http://localhost:3000/card/',
  search: 'http://localhost:3000/search/',
  searchText: 'http://localhost:3000/searchtext/'
};
