import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { CardSearchService } from '../card-search/card-search.service';
import { NewCardSearchService } from '../card-search/new-card-search.service';

@Component({
  selector: 'app-card-search',
  templateUrl: './card-search.component.html',
  styleUrls: ['./card-search.component.css']
})

export class CardSearchComponent implements OnInit {
  model = { name: '' };
  p: number;

  results: string[] = [''];
  searchTerm$ = new Subject<string>();

  constructor(private cardsearchservice: CardSearchService,
              private newcardsearchservice: NewCardSearchService) {

    this.newcardsearchservice.search(this.searchTerm$)
      .subscribe(results => {
          this.results = results || [''];
    });
  }

  getCard(name: string): void {
    this.cardsearchservice.getCard(name);
  }

  onSubmit(): void {
    this.getCard(this.model.name);
    this.clearForm();
  }

  clearForm(): void {
    this.model.name = '';
    this.results = [''];
  }

  ngOnInit() {
    // this.getCard('Cryptic Command');
  }

}
