import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Card } from '../Card';

import { CardSearchService } from './card-search.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import { devUrl, url } from '../urlList';

@Injectable()
export class CardTextSearchService {
  private cardUrl = url.searchText;
  private devCardUrl = devUrl.searchText;
  private cards: Card[] = [];

  constructor(private http: Http,
              private cardsearchservice: CardSearchService) { }

  search(terms: Observable<string>) {
    return terms.debounceTime(300)
      .distinctUntilChanged()
      .switchMap(term => {
        return this.searchEntries(term);
      });
  }

  searchEntries(term) {
    return this.http
      .get(this.cardUrl + term)
      .map(res => res.json());
  }
}
