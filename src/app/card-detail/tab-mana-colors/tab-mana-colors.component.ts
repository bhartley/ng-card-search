import { Component, OnInit } from '@angular/core';
import { Card } from '../../Card';
import { CardSearchService } from '../../card-search/card-search.service';

@Component({
  selector: 'app-tab-mana-colors',
  templateUrl: './tab-mana-colors.component.html'
})

export class TabManaColorsComponent implements OnInit {
  cardOne: Card;

  constructor(private cardsearchservice: CardSearchService) {
  }

  ngOnInit() {
    this.cardOne = this.cardsearchservice.getCurrentCard();
  }

}
