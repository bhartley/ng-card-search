import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Card } from '../Card';
import { CardSearchService } from '../card-search/card-search.service';

@Component({
  selector: 'app-card-detail',
  templateUrl: './card-detail.component.html',
  providers: [ ]
})

export class CardDetailComponent implements OnDestroy {
  cardOne: Card;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private search: CardSearchService) {

    this.route.url.subscribe((data: any) => {
      if (data && data[0] && data[0].path === 'byname') {
        const navto: string[] = [];
        for (let i = 0; i < data.length; i++) {
          navto.push(data[i].path);
        }
        navto.push('tab-types');
        this.router.navigate(navto);
      }
    });

    this.cardOne = this.search.getCurrentCard();
    this.search.cardUpdated.subscribe((card: Card) => {
      this.cardOne = card;
    });

  }

  ngOnDestroy() {
    this.search.clearCurrentCard();
  }

}
