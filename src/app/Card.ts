export interface Card {
  layout?: String;
  name?: String;
  manaCost?: String;
  cmc?: Number;
  colors?: String[];
  type?: String;
  types?: String[];
  subtypes?: String[];
  text?: String;
  power?: String;
  toughness?: String;
  imageName?: String;
  colorIdentity?: String[];
}
